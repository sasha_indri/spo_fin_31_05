package spo;

public class Token {
	 private String name;
	    private String value;
	    private int numLine;

	    public Token(String name, String value, int numLine) {
	        this.name = name;
	        this.value = value;
	        this.numLine= numLine;
	    }

	    public String getName() {
	        return name;
	    }
	    // здесь мы явно указываем что переменной name которая находится в объекте
        // мы присваиваем полученое значение параметра name (т.е. this.name и n это две разные переменные)
	    
	    public void setName(String name) {
	        this.name = name; // указатель на обЪект 
	    }

	    public String getValue() {
	        return value;
	    }
	    
	    public int getNumLine() {
	        return this.numLine;
	    }

	    public void setValue(String value) {
	        this.value = value;
	    }

	    @Override
	    public String toString() {  // создание структуры списка токенов для принта 
	        return "Token{" +
	                "name='" + name + '\'' +
	                ", value='" + value + '\'' +
	                '}';
	    }

    
            
            int getOpPriority() {
                String str =this.value;
		if (str.equals("(")) 
                    return 10;
		if (str.equals("+") || str.equals("-")) 
                    return 2;
		if (str.equals("*") || str.equals("/")||str.equals("u-")) 
                    return 3;
                if (str.equals(")"))
                    return 1;
		return 4;
	}
            
}
