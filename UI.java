package spo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class UI {
	

	static UtilsHelper utilsHelper = new UtilsHelper();

    public static void main(String[] args) throws Exception {

       validTest();	 // запуск программы

}

    public static void validTest() throws Exception { 
        utilsHelper.printFile("build/classes/valid.input");   // вывод на печать содержимого файла 
        process("build/classes/valid.input");       
    }
    
    public static void inValidTest() throws Exception {
        utilsHelper.printFile("build/classes/invalid.input");
        process("build/classes/invalid.input");
    }
    
    public static void process(String fileName) throws Exception {
        List<Token> TEMP = new ArrayList<Token>();
        
        Lexer lexer = new Lexer();   // создание н. обекта лексер
        lexer.processInput(fileName); // создание обекта 
        List<Token> tokens = lexer.getTokens(); // получить список токенов от лексера 
        Parser parser = new Parser(); // обЪект парсер
        parser.setTokens(tokens); // вывод полученных токенов 
        parser.lang();  // вывод считанных токенов 
        
        System.out.print("\n");
        Parser parser_P = new Parser(); // обЪект парсер
        parser_P.setTokens(tokens);
        TEMP = parser_P.getPoliz();
        
        System.out.print("\n");
        PProcessor PolisProc = new PProcessor();
        PolisProc.setPoliz(TEMP);
       // ToPoliz poliz = new ToPoliz(); // обЪект парсер
       // poliz.setTokens(tokens); // вывод полученных токенов 
       // poliz.getPoliz();
    }
    
}