package spo;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	List<Token> tokens = new ArrayList<Token>();

    String accum="";
    private int dl=0;
    private boolean sbit=false;
    private int numst=0;

    private Pattern sm = Pattern.compile("^;$");
    private Pattern var_kw = Pattern.compile("^var$");
    private Pattern assign_op = Pattern.compile("^=$");
    
    private Pattern addOp = Pattern.compile("^\\+$");
    private Pattern decOp = Pattern.compile("^\\-$");
    private Pattern multOp = Pattern.compile("^\\*$");
    private Pattern divOp = Pattern.compile("^\\/$");
    private Pattern digit = Pattern.compile("^0|[1-9]{1}[0-9]*$");
    private Pattern var = Pattern.compile("^[a-zA-Z]*$");
    private Pattern ws = Pattern.compile("^\\s*$");
    private Pattern open = Pattern.compile("^\\($");
    private Pattern close = Pattern.compile("^\\)$");
    
    private Pattern while_loop = Pattern.compile("^while$");
    private Pattern compOp_1 = Pattern.compile("^\\>|<$");
    private Pattern compOp_2 = Pattern.compile("^\\>=|<=$");
    private Pattern f_open = Pattern.compile("^\\{$");
    private Pattern f_close = Pattern.compile("^\\}$");
  

    private Map<String, Pattern> commonTerminals = new HashMap<String, Pattern>();  // создание хеш-таблицы

    private String currentLucky = null;

    private int i;

    public static final String VAR_KW = "VAR_KW";
    public static final String SM = "SM";
    
    public static final String ASSIGN_OP = "ASSIGN_OP";
    public static final String ADD_OP = "ADD_OP";// 
    public static final String DEC_OP = "DEC_OP";//
    public static final String MULTI_OP = "MULTI_OP";
    public static final String DIV_OP = "DIV_OP";
    public static final String DIGIT = "DIGIT";
    public static final String VAR_NAME = "VAR_NAME";
    public static final String WS = "WS";
    public static final String OPEN = "OPEN";
    public static final String CLOSE = "CLOSE";
    
    public static final String WHILE_LOOP = "WHILE";
    public static final String COMP_1 = "COMP_1";
    public static final String COMP_2 = "COMP_2";
    public static final String F_OPEN = "F_OPEN";
    public static final String F_CLOSE = "F_CLOSE";
    

    public Lexer(){
    
    	commonTerminals.put(Lexer.VAR_KW, var_kw);
    	commonTerminals.put(Lexer.SM, sm);
    	commonTerminals.put(Lexer.ASSIGN_OP, assign_op);

    	commonTerminals.put(Lexer.ADD_OP, addOp);
        commonTerminals.put(Lexer.DEC_OP, decOp);
        commonTerminals.put(Lexer.MULTI_OP, multOp);
        commonTerminals.put(Lexer.DIV_OP, divOp);
        commonTerminals.put(Lexer.DIGIT, digit);
        commonTerminals.put(Lexer.VAR_NAME, var);
        commonTerminals.put(Lexer.WS, ws);
        commonTerminals.put(Lexer.OPEN, open);
        commonTerminals.put(Lexer.CLOSE, close);
        
        commonTerminals.put(Lexer.WHILE_LOOP, while_loop);
        commonTerminals.put(Lexer.COMP_1, compOp_1);
        commonTerminals.put(Lexer.COMP_2, compOp_2);
        commonTerminals.put(Lexer.F_OPEN,f_open);
        commonTerminals.put(Lexer.F_CLOSE,f_close);
        

        
    }
    public void processInput(String fileName) throws IOException {

        File file = new File(fileName);   // чтение из файла строк 
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line;
        while( (( line = bufferedReader.readLine() ) != null)&&!sbit ) {
        	dl = line.length();
            processLine(line);
            numst++;
        }
        System.out.println("TOKEN("
                + currentLucky
                + ") recognized with value : "
                + accum
        );
        tokens.add(new Token(currentLucky, accum, numst));

        System.out.println("\n\t\t\tПары ключ/значение");
        System.out.println();
        for (Token token: tokens) {
            System.out.println(token);
        }

    }

    private void processLine(String line) {

        for (i=0; i<line.length(); i++) {
            accum = accum + line.charAt(i); // разбиение строки на символы и их запись в аккум 
            processAccum(i);
            
        }
        
    }

    private void processAccum(int k) {
        boolean found = false;
        boolean f1=false, 
                f2=false;
        
        for ( String regExpName : commonTerminals.keySet() ) {  // получение ключей из хеш_таблицы 
            Pattern currentPattern = commonTerminals.get(regExpName); // получение значения ключа 
            Matcher m = currentPattern.matcher(accum); // считанные символы сравниваются со значением ключа пол. из таблицы
            if(dl-1==k){
            	f1=true;
            	dl=0;
            }
            if( m.matches() ) { // в случае совпадения 
                currentLucky = regExpName; // записать ключ 
                found = true; // верное значение, считан удачно
            }
        }
        
        if (currentLucky != null && !found){  // вывод в случае успешного определения 
        	
            System.out.println("TOKEN("
                    + currentLucky
                    + ") recognized with value : "
                    + accum.substring(0,accum.length()-1));
            f2=true;
            tokens.add( new Token( currentLucky, accum.substring(0,accum.length()-1 ), numst) ); // создать токен и записать его в список токенов
            i--;
            accum = "";
            currentLucky = null;
        }
        else{
        	if(f1&&!f2){
        		System.out.println("ошибка неизвестный символ"+accum+" строка> "+numst+"\n");
        		sbit=true;
        		return ;
        	}
        }

    }

    public List<Token> getTokens() {
        return tokens;
    }
}
