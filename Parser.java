package spo;

import java.util.*;
import java.lang.*;
import java.io.*;

public class Parser {

	private List<Token> tokens;
	private Iterator<Token> iteratorTokens;
	private Token currentToken;
	private Token currentTokenVn;
        public  Token pred;
        public  Token pred_oP;
	private int bktCounter;
	private List<Token> tokens_vn = new ArrayList<Token>();
	private int counntVar = 0;
        List<Token> stroki = new ArrayList<Token>();

	public void setTokens(List<Token> tokens) {
		
		this.tokens = tokens; // создать список токенов
		iteratorTokens = this.tokens.iterator();
	}

	public void lang() throws Exception {
		boolean activated = false;
		System.out.println("\n///////////////////////////////////\n\n\t\t\tTokens in Parser.java:\n");
		for (Token token : tokens) {
			System.out.println(token);
		}

		while (nextToken()) {
			if (expr()) {
				if (!sm()) {
					throw new Exception("Error at line:" 
                                                + currentToken.getNumLine() 
                                                + ".\n\t\t \';\" expected, but \""
						+ currentToken + "\" found.");
				}
			} else {
				throw new Exception("Error at line:" 
                                        + currentToken.getNumLine() 
                                        + ".\n\t\t \"expr\" expected, but \""
					+ currentToken + "\" found.");
			}
			activated = true;
		}
		System.out.println("completed!!");
		if (!activated) {
			throw new Exception("Error. No expr found.");
		}
	}

	private boolean expr() throws Exception {
		if (!(declare() || assign() || while_loop())) {
			throw new Exception("Error at line:" 
                                + currentToken.getNumLine() 
                                + ".\n\t\t \"declare or assign\" expected"
					+ declare());// ,
			// but
			// \""+currentToken+"\"
			// found.");
		}
		return true;
	}

	private boolean declare() throws Exception {
		if (varKw()) {
			nextToken();
			if (varName()) { 
                            if(!table_set()){
			throw new Exception("ERROR: variable " + currentToken.getValue() + " is declared up");
                                
                            }else set_table_vn();// занесение в таблицу имя переменной токена
				nextToken(); // для ";"
			} else {
				throw new Exception("Error at line:" 
                                        + currentToken.getNumLine()
					+ ".\n\t\t \"var name\" expected, but \"" 
                                        + currentToken + "\" found.");
			}
		} else {
			return false;
		}

		// создать таблицу объявленных переменных

		return true;
	}

	private boolean assign() throws Exception {
		bktCounter = 0;
		if (varName()) { // a
                    if(table_set()){
                       throw new  Exception("Error, variable is not declared");
                    }else
			nextToken();
			if (assignOp()) { // =
				nextToken();
                                if(sm()){
                                    throw new Exception("Error, expected expression");
                                }
				if (!smth()) {
					throw new Exception("Error at line:" 
                                                + currentToken.getNumLine()
						+ ".\n\t\t \"var name, digit, or its with op\" expected, but \"" + currentToken
						+ "\" found.");
				}
                                error_bracket();
			} else {
				throw new Exception("Error at line:" 
                                        + currentToken.getNumLine()
					+ ".\n\t\t \"assign op\" expected, but \"" 
                                        + currentToken + "\" found.");
			}

		} else {
			return false;
		}
		return true;
	}

	private boolean smth() throws Exception {
            
                    if (stmt_unit_open()){
			//if(currentToken.getName().equals(Lexer.DEC_OP)||smthUnit()) //a || 100 || -
                            if(smthUnit()){                                     
                                nextToken();
                        }else if(currentToken.getName().equals(Lexer.DEC_OP)){  // - 
				nextToken();
                              if(!smthUnit()){                                     // a || 100
                                throw new Exception("Var, digit expexted, but " + currentToken + " found");
                        }

			} else {
				throw new Exception("Var, digit or '-' expexted, but " + currentToken + " found");
			}
                        
                    }
                    
                    if (stmt_unit_close()) {
                       //nextToken();
                       if(sm()){
                           return true;
                       }
                       if(smthUnit()){
                           throw new Exception("ожидалась операция ");
                       }
                    } 
                    
                    
                    if (op()){
                        nextToken();
                        if (sm()||op()){
                            throw new Exception("Ожидался значение или переменная ");
                        }else if(stmt_unit_close()){
                            throw new Exception("Ожидался значение или переменная ");
                        }
                        smth();
                    }else{
                      //if (sm()){
                      //  throw new Exception("Ожидался оператор или ; ");
                      //  }
                    }
                    
                    if(smthUnit()){
                        nextToken();
                        if(sm()){
                           return true;
                       }else if(stmt_unit_open()){
                             throw new Exception("ожидался знак операции");  
                             }
                      smth();
                        
                    }
                    
                    return true;
        }
	
       
                

        
        /*private boolean smth1() throws Exception {
		if (stmt_unit_open()) { // sm() =;  assignOp() = =  op() = +-*  nextToken() unar() = - smthUnit() 
                    if(unar()|| smthUnit()){
                        nextToken();
                    }
			
		
        }
            return true;
        }
        
        */
	public boolean stmt_unit_open() throws Exception {
            if(open()&&iteratorTokens.hasNext()){
            while (open()) { // (
			bktCounter++;

			nextToken();
		}
            return true;
            }
		return false;
	}


        
        public boolean stmt_unit_close() throws Exception {
            if(close()){
            while (close()) {// )
			bktCounter--;
                        
                        if(!iteratorTokens.hasNext()){
                throw new Exception("Неожиданный конец выражения, ожидалось продолжение или ';'");
            }
			nextToken();
                        
                        
		}
		return true;
            }
            return false;
	}//iteratorTokens.hasNext()
        
        
        
        
        
        

	private void error_bracket() throws Exception {
		if (bktCounter < 0) {
			bktCounter = bktCounter * (-1);
			throw new Exception("ERROR: " + bktCounter + " opening bracket(s) missing.");
		} else if (bktCounter > 0) {
			throw new Exception("ERROR: " + bktCounter + " closing bracket(s) missing.");
		}

	}

	private void set_table_vn() { // создание таблицы обявленных переменных
            

		tokens_vn.add(new Token(currentToken.getName(), currentToken.getValue(), counntVar));
		counntVar++;

	}

	private boolean sm() {
		return currentToken.getName().equals(Lexer.SM);
	}

	private boolean smthUnit() throws Exception {
            
            if(iteratorTokens.hasNext()){
		// проверка обявленности переменных созд
		boolean flag_var = true;
		if (currentToken.getName().equals(Lexer.VAR_NAME)) {
			for (Token token : tokens_vn) {
				if (token.getValue().equals(currentToken.getValue())) {
					flag_var = false;
					// System.out.println(token.getValue()+" "+
					// currentToken.getValue());
				}
			}
			if (flag_var) {
				throw new Exception("ERROR: variable " + currentToken.getValue() + " is not declared");
			}

		}

		return (currentToken.getName().equals(Lexer.DIGIT) || currentToken.getName().equals(Lexer.VAR_NAME));
	}//else{
                //if(!sm())
                //throw new Exception("Неожиданный конец выражения");
//}
            return false;
        }    
        
        
        private boolean table_set() throws Exception{
            boolean flag_var = true;
		if (currentToken.getName().equals(Lexer.VAR_NAME)) {
			for (Token token : tokens_vn) {
				if (token.getValue().equals(currentToken.getValue())) {
					flag_var = false;
					// System.out.println(token.getValue()+" "+
					// currentToken.getValue());
				}
			}
			if (flag_var) {
                         return true;   
			//	throw new Exception("ERROR: variable " + currentToken.getValue() + " is not declared ");
			}

		}
         return false;
        }

        private boolean smthUnit_poliz() throws Exception{
         return (currentToken.getName().equals(Lexer.DIGIT) || currentToken.getName().equals(Lexer.VAR_NAME));   
        }
	private boolean varName() {
		return currentToken.getName().equals(Lexer.VAR_NAME);
	}
        
        private boolean whileLoop() {
		return currentToken.getName().equals(Lexer.WHILE_LOOP);
	}

	private boolean assignOp() {
		return currentToken.getName().equals(Lexer.ASSIGN_OP);
	}

	private boolean varKw() {
		return currentToken.getName().equals(Lexer.VAR_KW);
	}

	private boolean open() {
		return currentToken.getName().equals(Lexer.OPEN);
	}

	private boolean close() {
		return currentToken.getName().equals(Lexer.CLOSE);
	}
        
        private boolean f_open() {
		return currentToken.getName().equals(Lexer.F_OPEN);
	}
        
        private boolean f_close() {
		return currentToken.getName().equals(Lexer.F_CLOSE);
	}

	private boolean op() {
		return (currentToken.getName().equals(Lexer.ADD_OP) || currentToken.getName().equals(Lexer.DEC_OP)||
                        currentToken.getName().equals(Lexer.MULTI_OP)|| currentToken.getName().equals(Lexer.DIV_OP));
	}
        
        private boolean op_comp() {
            return (currentToken.getName().equals(Lexer.COMP_1) || currentToken.getName().equals(Lexer.COMP_2) || currentToken.getName().equals(Lexer.EQUALITY));   
        }

	private boolean nextToken() throws Exception {
		while (iteratorTokens.hasNext()) {
			do {
				currentToken = iteratorTokens.next();
			} while (iteratorTokens.hasNext()&& currentToken.getName().equals(Lexer.WS));
			return true;
		}
                
		return false;
	}
        
        private boolean while_loop() throws Exception{
            if(whileLoop()){
                nextToken();
                if(open()){
                    nextToken();
                    if(comp()){
                        if(close()){
                            nextToken();
                            if(f_open()){
                                nextToken();
                                while(assign()||while_loop()){
                                    nextToken();
                                }
                                if(f_close()){
                                    nextToken();
                                    return true;
                                }else{
                                    throw new Exception("ERROR: Figure close bracket expected, but"+ currentToken +" found");
                                }
                            }else{
                                throw new Exception("ERROR: Figure open bracket expected, but"+ currentToken +" found");
                            }
                        }else{
                            throw new Exception("ERROR: Close bracket expected, but"+ currentToken +" found");
                        }
                        
                    }else{
                        throw new Exception("ERROR: Expression expected, but"+ currentToken +" found");
                    }
                    
                    
                }else{
                   throw new Exception("ERROR: Open bracket expected, but"+ currentToken +" found"); 
                }
                
            }else{
            return false;
            }
        ///    return true;
        }
        
        
        private boolean comp() throws Exception{
            
            if(smthUnit()){
                nextToken();
                if(op_comp()){
                    nextToken();
                    if(smthUnit()){
                        nextToken();
                        return true;
                    }else{
                        throw new Exception("ERROR: Variable or digit expected, but"+ currentToken +" found");
                    }
                }else{
                    throw new Exception("ERROR: Compare operation expected, but"+ currentToken +" found");
                }
            }else{
                throw new Exception("ERROR: Variable or digit expected, but"+ currentToken +" found");
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        private boolean unar() {
		return currentToken.getName().equals(Lexer.DEC_OP);
	}
        
        private boolean op_pred() {
		return (pred.getName().equals(Lexer.ADD_OP) || pred.getName().equals(Lexer.DEC_OP)||
                        pred.getName().equals(Lexer.MULTI_OP)|| pred.getName().equals(Lexer.DIV_OP));
	}
        
        private boolean op_stack(Token st_peek) {
		return (st_peek.getName().equals(Lexer.ADD_OP) || st_peek.getName().equals(Lexer.DEC_OP)||
			st_peek.getName().equals(Lexer.MULTI_OP) || st_peek.getName().equals(Lexer.DIV_OP) || st_peek.getName().equals("Unar"));
	}
        
        public void copy_list(List<Token> postf) {
		
		this.stroki = postf; // создать список токенов
		iteratorTokens = this.stroki.iterator();
	}



//////////////////////////////////////////////////////////////////////////
public List<Token> getPoliz() throws Exception {
    
		List<Token> poliz = new ArrayList<Token>();
		Stack<Token> stack = new Stack<Token>();
                Token assing = null;
		Token curr;
                Token razd = null;
            
                
                boolean flag;
                while(iteratorTokens.hasNext()){
                    
		while(iteratorTokens.hasNext()){
                   // System.out.println(currentToken);
//System.out.println("ТЕКУЩИЙ ИТЕРАТОР ТОКЕН - "+iteratorTokens.next().toString());
                    curr = iteratorTokens.next();
                    currentToken = curr;
                    
                    if(!iteratorTokens.hasNext()&&op()){
                    //    System.out.println(currentToken);
                        System.out.println("Некорректное выражение.");
				flag = false;
				return poliz;
                    }
                    if (currentToken.getName().equals(Lexer.WS)){ 
                        continue;
             
                    }
                    
                    if(assignOp()){
                        
                      assing=currentToken;
                      pred=currentToken;
                       curr = iteratorTokens.next();
                        currentToken = curr;
                    }
                    
                    if (currentToken.getName().equals(Lexer.WS)){ 
                        continue;
             
                    }
                    
                    if(varKw()||sm()){
// выгрузка постфикса
                        if(!sm()){
                            //poliz.add(curr);
                         //iteratorTokens.next();
                            
                            iteratorTokens.next();
                         curr = iteratorTokens.next();  
                         poliz.add(curr);
                         poliz.add(new Token (Lexer.DIGIT,"0", 1));
                         poliz.add(new Token (Lexer.ASSIGN_OP,"=", 2));
                         poliz.add(new Token("DIVID", "  ///  ", 0)); 
                         
                         curr = iteratorTokens.next();
                        }else{
                        
                            if(!poliz.isEmpty())
                             break;   
                        }
                       continue;
                        
                    }
                    else if (op()||open()||close()) {
                        if (open()){
                            if(pred_oP!=null){
                            if(pred_oP.getName().equals("Unar")){
                                if(stack.isEmpty())
                                poliz.add(stack.pop());
                            }
                            }
                            pred_oP = currentToken;
                            stack.push(curr);
                           System.out.println(stack);
                        } else if (close()) {
                           
                            pred_oP = currentToken;
                            while (!stack.peek().getName().equals(Lexer.OPEN)) {
                                
                             //   System.out.println(currentToken);
                             //   System.out.println(stack);
                                
                                poliz.add(stack.pop());
                                 
                                if (stack.isEmpty()) {
                                    
                                    System.out.println("");
                                    
                                    System.out.println(stack);
                                    System.out.println("");
                                    System.out.println(currentToken);
                                    System.out.println("");
                                    System.out.println(poliz);
                                    
                                    System.out.println("Скобки не согласованы.");
                                    flag = false;
                                    return poliz;
				}
                                
                            }
                            //stack.pop();
                            System.out.println("//////////////////"+stack.pop());
                                    
                            if (!stack.isEmpty()) { //функции добавить 
                            //poliz.add(stack.pop());
                            }
                            
                        }else {
                            if (unar() && (pred.getName().equals("") || (pred.getName().equals(Lexer.OPEN)&& unar()) || pred.getName().equals(Lexer.ASSIGN_OP))) {        // ASSING
                                                // унарный минус        op_pred()  &&
						curr.setName("Unar"); 
                                                curr.setValue("u-");
                                                //poliz.add(curr);
                                                pred_oP = curr;//entToken
                            }else {
                                    while (!stack.isEmpty() && (curr.getOpPriority() <= stack.peek().getOpPriority())) { // работа тоько со знаками pred_oP.getOpPriority()
                                      System.out.println(poliz+" до");
                                     //
                                      if(stack.peek().getName().equals(Lexer.OPEN)){
                                          break;
                                      }
                                     
                                    //stack.push(curr);
                                    poliz.add(stack.pop());
                                      
                                    
                                    System.out.println(poliz+" после");
                                    //priority=curr.getOpPriority();  // не изменяется приоритет ри 
                                    
                                    }	
                                    pred_oP = currentToken;
				}
                            stack.push(curr);
                            
                            
                        }
                            
                     
                    }else {         ///попадаем если числа
                        if(!open())
				poliz.add(curr); 
                             ///   System.out.println(poliz);////////////////////////////
			}
                    pred = curr;
                }
                
              //  stack.add(assing);
                
                while (!stack.isEmpty()) {
			if (op_stack(stack.peek())) {
                            poliz.add(stack.pop());
                        }
                        
			else {
				System.out.println("Скобки не согласованы.");
				flag = false;
				return poliz;
			}
		}
                if(stack.isEmpty()&&!Lexer.F_CLOSE.equals(pred.getName())){
                    poliz.add(assing);
                }
                poliz.add(new Token("DIVID", "  ///  ", 0)); 
                
               // copy_list(poliz);
               // System.out.println("!!!!!!!!!!!!!!!!!!!!"+stroki);
                //poliz = null;
                
                 
                }
                
                

                
                System.out.println("\n///////////////////////////////////\n\n\t\t\tPOLIZ:\n");
		for (Token token : poliz) {
			System.out.print(" " + token.getValue());
		}
                
		return poliz;
     }
}
