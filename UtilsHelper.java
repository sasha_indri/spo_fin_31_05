package spo;
import java.io.*;

public class UtilsHelper {

	public void printFile(String fileName) throws IOException {
		
		 File file = new File(fileName);

	        Reader reader = new FileReader(file);

	        BufferedReader bufferedReader = new BufferedReader(reader);
                
                String line;
                
	        int lineNumber = 0;

	        while( ( line =bufferedReader.readLine()) != null ) {
	            System.out.println(lineNumber + ": " + line);
	            lineNumber++;
	}
                System.out.println();
                System.out.println("\n\t\t\t Вывод распознанных токенов\n");
                

	}
}