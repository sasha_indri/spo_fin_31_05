package spo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class PProcessor {

    private Stack<Object> stack = new Stack<Object>();
    private Map<String, Double> varMap = new HashMap<String, Double>();
    private List<Token> poliz;
    Token currentPfxToken;
    int index;
   
    int temp_bracket = 0;
    double size;
    double tmpValue1;
    Object tmpValue2;

  /* public PProcessor(List<Token> srcPoliz) throws Exception {
        stack = new Stack<Object>();
         varMap = new HashMap<String, Integer>();
       this.poliz = srcPoliz;
       
       for (Token token : poliz) {
			System.out.print(" " + token.getValue());
		}
        
         calcPoliz();
    }*/
    
     void setPoliz(List<Token> Poliz) throws Exception{
         this.poliz = Poliz;
        //for (Token token : poliz) {
        //System.out.print(" " + token.getValue());
	//	}
         calcPoliz();
         //return null;  
     }

    public void calcPoliz() throws Exception {
        index = 0;
        size = this.poliz.size();
        
        while (index < size) {
            currentPfxToken = this.poliz.get(index);
            if (currentPfxToken.getName().equals(Lexer.DIGIT)) {
                stack.push(Double.parseDouble(currentPfxToken.getValue()));
            } else if (currentPfxToken.getName().equals(Lexer.VAR_NAME)) {
                // создать таблицу 
               // varMap.put(currentPfxToken.getValue(), 0.0);
                stack.push(currentPfxToken.getValue());
            } else if (currentPfxToken.getName().equals(Lexer.ASSIGN_OP)) {
                tmpValue1 = getOperand(stack.pop());
                varMap.put((String) stack.pop(), tmpValue1);
            } else if (currentPfxToken.getName().equals(Lexer.ADD_OP)) {
                stack.push(getOperand(stack.pop()) + getOperand(stack.pop()));
            } else if (currentPfxToken.getName().equals(Lexer.DEC_OP)) {
                tmpValue1 = getOperand(stack.pop());
                stack.push(getOperand(stack.pop()) - tmpValue1);
            } else if (currentPfxToken.getName().equals(Lexer.MULTI_OP)) {
                stack.push(getOperand(stack.pop()) * getOperand(stack.pop()));
            }else if (currentPfxToken.getName().equals(Lexer.DIV_OP)) {
                Double temp = getOperand(stack.pop());
                stack.push(getOperand(stack.pop())/temp);
            }else if (currentPfxToken.getName().equals("Unar")) {
                stack.push(-getOperand(stack.pop()));
                
            }else if(currentPfxToken.getName().equals(Lexer.WHILE_LOOP)){
                /*   index_1 = index+1;   //первый операнд условия
                    temp_bracket = skip_br(index_1);
                   int temp_in=index+1;
                //System.out.println("*******************************" +sravn());  
                  while(sravn()){
                  // index_1=temp_in;
                    //System.out.println("****************************цикл "); 
                    getCondit();
                    index_1=temp_in; 
                   }
                 
                  */ 
                
                   index = while_method(index);
                   
                 //  System.out.println("index_1-----------------\n" +index_1 + "\t" + this.poliz.get(index_1));
                //  int ass = while_method(index_1);
                  
                // System.out.println("temp_in*************\n" +temp_in + "\t" + this.poliz.get(temp_in));
                  
              //  System.out.println("index_1*************\n" +index_1 + "\t" + this.poliz.get(index_1));
                
                System.out.println("index*************\n" +index + "\t" + this.poliz.get(index));
                
                System.out.println("temp_bracket*************\n" +temp_bracket + "\t" + this.poliz.get(temp_bracket));
                   
               // index=index_1+temp_bracket-3;//   index = while + }
                
               System.out.println("temp_bracket*************\n" +temp_bracket + "\t" + this.poliz.get(index));
            }
//			System.out.println(stack);
            index++;
        }
        System.out.println("\n" + varMap);
    }
    

    private double getOperand(Object tmp) throws Exception {
        if (tmp.getClass().equals(String.class) ) {
            if (varMap.containsKey((String) tmp)) {//varMap.containsKey((String) tmp)
                return varMap.get((String) tmp);

            }else {
                // если это число то перевести в инт и вывести 
                throw new Exception("\n\t\tMissing declare of \"" + (String) tmp + "\"");
            }
        }
        return (Double) tmp;
    }
    
    private boolean getCondit(int local_ind) throws Exception {
        Token currentPfxToken_1;
        currentPfxToken_1 = this.poliz.get(local_ind);
//        System.out.println("**************************** заход "); 
        
        while (!currentPfxToken_1.getName().equals(Lexer.F_CLOSE)) {
            currentPfxToken_1 = this.poliz.get(local_ind);
             
            if(currentPfxToken_1.getName().equals(Lexer.F_OPEN)){
                
            }else if (currentPfxToken_1.getName().equals(Lexer.DIGIT)) {
                stack.push(Double.parseDouble(currentPfxToken_1.getValue()));
            } else if (currentPfxToken_1.getName().equals(Lexer.VAR_NAME)) {
                stack.push(currentPfxToken_1.getValue());
            } else if (currentPfxToken_1.getName().equals(Lexer.ASSIGN_OP)) {
                tmpValue1 = getOperand(stack.pop());
                varMap.put((String) stack.pop(), tmpValue1);
            } else if (currentPfxToken_1.getName().equals(Lexer.ADD_OP)) {
                stack.push(getOperand(stack.pop()) + getOperand(stack.pop()));
            } else if (currentPfxToken_1.getName().equals(Lexer.DEC_OP)) {
                tmpValue1 = getOperand(stack.pop());
                stack.push(getOperand(stack.pop()) - tmpValue1);
            } else if (currentPfxToken_1.getName().equals(Lexer.MULTI_OP)) {
                stack.push(getOperand(stack.pop()) * getOperand(stack.pop()));
            }else if (currentPfxToken_1.getName().equals(Lexer.DIV_OP)) {
                Double temp = getOperand(stack.pop());
                stack.push(getOperand(stack.pop())/temp);
            }else if (currentPfxToken_1.getName().equals("Unar")) {
                stack.push(-getOperand(stack.pop()));
                
            }else if(currentPfxToken.getName().equals(Lexer.WHILE_LOOP)){
                // local_ind = while_method(local_ind);
            }
          local_ind++;
//          System.out.println("*******************************" +currentPfxToken_1);
        }
        
        return false; 
    }
    boolean sravn(int p1) throws Exception{
        // int p1=index_1;
         int iop=p1+1;
         int p2=p1+2;
         boolean fl;
         
         
         System.out.println("*******************************сравнение " +iop+" "+this.poliz.get(iop).getValue());
         System.out.println("*******************************" +p1+" "+this.poliz.get(p2).getValue());
          
         
        if (this.poliz.get(iop).getValue().equals(">")) {
            System.out.println("****************************заход в сравнение");
            if (!(poliz.get(p1).getName().equals(Lexer.DIGIT)) && poliz.get(p2).getName().equals(Lexer.DIGIT)) {
               fl = ((getOperand(poliz.get(p1).getValue())) > (Double.parseDouble(poliz.get(p2).getValue())));
                return (fl);
            } else if(poliz.get(p1).getName().equals(Lexer.DIGIT) && !(poliz.get(p2).getName().equals(Lexer.DIGIT))) {
                fl = (Double.parseDouble(poliz.get(p1).getValue())) > (getOperand(poliz.get(p2).getValue()));
              
                return (fl);
            }else{
                fl = ((getOperand(poliz.get(p1).getValue())) >(getOperand(poliz.get(p2).getValue())));
                return (fl);
            }

            
        } else if (this.poliz.get(iop).getValue().equals("<")) {
        //    System.out.println("****************************заход в сравнение");
            if (!(poliz.get(p1).getName().equals(Lexer.DIGIT)) && poliz.get(p2).getName().equals(Lexer.DIGIT)) {
               fl = ((getOperand(poliz.get(p1).getValue())) < (Double.parseDouble(poliz.get(p2).getValue())));
                return (fl);
            } else if(poliz.get(p1).getName().equals(Lexer.DIGIT) && !(poliz.get(p2).getName().equals(Lexer.DIGIT))) {
                fl = (Double.parseDouble(poliz.get(p1).getValue())) < (getOperand(poliz.get(p2).getValue()));
              
                return (fl);
            }else{
                fl = ((getOperand(poliz.get(p1).getValue())) < (getOperand(poliz.get(p2).getValue())));
                return (fl);
            }

        } else if (this.poliz.get(iop).getName().equals(Lexer.EQUALITY)) {
            if (!(poliz.get(p1).getName().equals(Lexer.DIGIT)) && poliz.get(p2).getName().equals(Lexer.DIGIT)) {
               fl = ((getOperand(poliz.get(p1).getValue())) == (Double.parseDouble(poliz.get(p2).getValue())));
                return (fl);
            } else if(poliz.get(p1).getName().equals(Lexer.DIGIT) && !(poliz.get(p2).getName().equals(Lexer.DIGIT))) {
                fl = (Double.parseDouble(poliz.get(p1).getValue())) == (getOperand(poliz.get(p2).getValue()));
              
                return (fl);
            }else{
                fl = ((getOperand(poliz.get(p1).getValue())) == (getOperand(poliz.get(p2).getValue())));
                return (fl);
            }
            
            
        } else if (this.poliz.get(iop).getValue().equals(">=")) {
            if (!(poliz.get(p1).getName().equals(Lexer.DIGIT)) && poliz.get(p2).getName().equals(Lexer.DIGIT)) {
               fl = ((getOperand(poliz.get(p1).getValue())) >= (Double.parseDouble(poliz.get(p2).getValue())));
                return (fl);
            } else if(poliz.get(p1).getName().equals(Lexer.DIGIT) && !(poliz.get(p2).getName().equals(Lexer.DIGIT))) {
                fl = (Double.parseDouble(poliz.get(p1).getValue())) >= (getOperand(poliz.get(p2).getValue()));
              
                return (fl);
            }else{
                fl = ((getOperand(poliz.get(p1).getValue())) >= (getOperand(poliz.get(p2).getValue())));
                return (fl);
            }
            
            
        } else if (this.poliz.get(iop).getValue().equals("<=")) {
            if (!(poliz.get(p1).getName().equals(Lexer.DIGIT)) && poliz.get(p2).getName().equals(Lexer.DIGIT)) {
               fl = ((getOperand(poliz.get(p1).getValue())) <= (Double.parseDouble(poliz.get(p2).getValue())));
                return (fl);
            } else if(poliz.get(p1).getName().equals(Lexer.DIGIT) && !(poliz.get(p2).getName().equals(Lexer.DIGIT))) {
                fl = (Double.parseDouble(poliz.get(p1).getValue())) <= (getOperand(poliz.get(p2).getValue()));
              
                return (fl);
            }else{
                fl = ((getOperand(poliz.get(p1).getValue())) <= (getOperand(poliz.get(p2).getValue())));
                return (fl);
            }
        }

        return false;    
    }
    
    private int skip_br(int prev_index) throws Exception {
        
        Token curr_Token;
        int i = prev_index;
        int kolvo_br=0;
        temp_bracket = 0;
        curr_Token = this.poliz.get(i);
       
        while (!curr_Token.getName().equals(Lexer.F_CLOSE) && kolvo_br==0) {
            curr_Token = this.poliz.get(i);
             if(curr_Token.getName().equals(Lexer.WHILE_LOOP)){
                kolvo_br++;
             }else if(curr_Token.getName().equals(Lexer.F_CLOSE)){
                 kolvo_br--;
             }
            temp_bracket = temp_bracket+1;
            System.out.println("**************************** заход в метод "+ temp_bracket + "///" +curr_Token);
            i++;
        }
        System.out.println("**************************** выход из метода "+ temp_bracket + "///" +curr_Token);
        return temp_bracket;
    }


    private int while_method(int index_1) throws Exception {
      //  int index_1;
    index_1++;   //первый операнд условия
                    temp_bracket = skip_br(index_1);
                   int temp_in=index_1;
                //System.out.println("*******************************" +sravn());  
                  while(sravn(index_1)){
                      index_1=index_1+3;
                      
                  // index_1=temp_in;
                    //System.out.println("****************************цикл "); 
                    getCondit(index_1);
                    index_1=temp_in; 
                   }
                  
       return index_1+temp_bracket-3;

}

}
